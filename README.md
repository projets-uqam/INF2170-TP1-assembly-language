THE BASES
===================================================
TP1 - E2016 - INF2170 


Summary
---------
* Converting decimal number to binary, octal and hexadecimal in PEP/8 assembly language


Technologies used:
------------------
* PEP/8 Machine Language Programming


PEP/8
----------
* Pep/8 is a virtual machine for writing machine language and assembly
language programs. 
* Programmed by: Chris Dimpfl and J. Stanley Warford


Execution example
-----------------
```sh
Bienvenue au programme Les BASES !!!

Veillez entrez un nombre decimal   (.) pour terminer: 87

nombre	binaire         	octal 	decimal	hexadecimal
+87	    0000000001010111	000127	+00087	0057

Veillez entrez un nombre decimal (.)  pour terminer: -01

nombre	binaire         	octal 	decimal	hexadecimal
-1	    1111111111111111	177777	-00001	FFFF

Veillez entrez un nombre decimal (.)  pour terminer: +30583

nombre	binaire         	octal 	decimal	hexadecimal
+30583	0111011101110111	073567	+30583	7777

Veillez entrez un nombre décimal (.)  pour terminer: -01029

nombre	binaire         	octal 	decimal	hexadecimal
-1029	1111101111111011	175773	-01029	FBFB

Veillez entrez un nombre décimal (.)  pour terminer: -25812

nombre	binaire         	octal 	decimal	hexadecimal
-25812	1001101100101100	115454	-25812	9B2C

Veillez entrez un nombre décimal (.)  pour terminer: .

Fin normale du programme.

```


License
--------
This project is licensed under the Apache License - see [here](https://www.gnu.org/licenses/gpl-3.0.en.html) for details
